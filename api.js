let api = (url, d, success, o) => {
    if (o === undefined) o = {};
    o = $.extend({
        dataType: 'json',
        method: 'GET',
        url: cfg.URL_API+url,
        data: d,
        success: success,
        error: (xhr, txt_status, err) => {
            console.log(txt_status);
            console.log(err);
        }
    }, o);

    return $.ajax(o);
};

